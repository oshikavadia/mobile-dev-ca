package com.oshikavadia.mobiledevca1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.preference.Preference;
import android.provider.Settings;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.content.Intent;

/**
 * Created by Oshi on 12-Nov-17.
 */

public class Utils {
    private static int sTheme;
//    public final static int THEME_DEFAULT = 0;
//    public final static int THEME_DARK = 1;

    static String getParthenonData() {
        return "<p>The <b>Parthenon</b> (<span class=\"nowrap\"><span class=\"IPA nopopups noexcerpt\"><a href=\"/wiki/Help:IPA/English\" title=\"Help:IPA/English\">/<span style=\"border-bottom:1px dotted\"><span title=\"/ˈ/: primary stress follows\">ˈ</span><span title=\"'p' in 'pie'\">p</span><span title=\"/ɑːr/: 'ar' in 'far'\">ɑːr</span><span title=\"/θ/: 'th' in 'thigh'\">θ</span><span title=\"/ə/: 'a' in 'about'\">ə</span><span title=\"/ˌ/: secondary stress follows\">ˌ</span><span title=\"'n' in 'nigh'\">n</span><span title=\"/ɒ/: 'o' in 'body'\">ɒ</span><span title=\"'n' in 'nigh'\">n</span><span title=\"/ˌ/: secondary stress follows\">ˌ</span></span> -<span style=\"border-bottom:1px dotted\"><span title=\"'n' in 'nigh'\">n</span><span title=\"/ən/: 'on' in 'button'\">ən</span></span>/</a></span></span>; <a href=\"/wiki/Ancient_Greek_language\" class=\"mw-redirect\" title=\"Ancient Greek language\">Ancient Greek</a>: <span lang=\"grc\" xml:lang=\"grc\"><a href=\"https://en.wiktionary.org/wiki/%CE%A0%CE%B1%CF%81%CE%B8%CE%B5%CE%BD%CF%8E%CE%BD#Ancient_Greek\" class=\"extiw\" title=\"wikt:Παρθενών\">Παρθενών</a></span>; <a href=\"/wiki/Modern_Greek_language\" class=\"mw-redirect\" title=\"Modern Greek language\">Modern Greek</a>: <span lang=\"ell\" xml:lang=\"ell\">Παρθενώνας</span>, <i>Parthenónas</i>) is a former <a href=\"/wiki/Ancient_Greek_temple\" title=\"Ancient Greek temple\">temple</a>,<sup id=\"cite_ref-Neils2005_4-0\" class=\"reference\"><a href=\"#cite_note-Neils2005-4\">[4]</a></sup><sup id=\"cite_ref-HambidgeFund1924_5-0\" class=\"reference\"><a href=\"#cite_note-HambidgeFund1924-5\">[5]</a></sup> on the <a href=\"/wiki/Acropolis_of_Athens\" title=\"Acropolis of Athens\">Athenian Acropolis</a>, <a href=\"/wiki/Greece\" title=\"Greece\">Greece</a>, dedicated to the <a href=\"/wiki/Greek_gods\" class=\"mw-redirect\" title=\"Greek gods\">goddess</a> <a href=\"/wiki/Athena\" title=\"Athena\">Athena</a>, whom the people of <a href=\"/wiki/Athens\" title=\"Athens\">Athens</a> considered their patron. Construction began in 447&nbsp;BC when the <a href=\"/wiki/Athenian_Empire\" class=\"mw-redirect\" title=\"Athenian Empire\">Athenian Empire</a> was at the peak of its power. It was completed in 438&nbsp;BC although decoration of the building continued until 432&nbsp;BC. It is the most important surviving building of <a href=\"/wiki/Classical_Greece\" title=\"Classical Greece\">Classical Greece</a>, generally considered the zenith of the <a href=\"/wiki/Doric_order\" title=\"Doric order\">Doric order</a>. Its decorative sculptures are considered some of the high points of <a href=\"/wiki/Art_in_Ancient_Greece\" class=\"mw-redirect\" title=\"Art in Ancient Greece\">Greek art</a>. The Parthenon is regarded as an enduring symbol of <a href=\"/wiki/Ancient_Greece\" title=\"Ancient Greece\">Ancient Greece</a>, <a href=\"/wiki/Athenian_democracy\" title=\"Athenian democracy\">Athenian democracy</a> and <a href=\"/wiki/Western_culture\" title=\"Western culture\">western civilization</a>,<sup id=\"cite_ref-Beard2010_6-0\" class=\"reference\"><a href=\"#cite_note-Beard2010-6\">[6]</a></sup> and one of the world's greatest cultural monuments. To the Athenians who built it, the Parthenon and other Periclean monuments of the Acropolis, were seen fundamentally as a celebration of Hellenic victory over the Persian invaders and as a thanksgiving to the gods for that victory.<sup id=\"cite_ref-:0_7-0\" class=\"reference\"><a href=\"#cite_note-:0-7\">[7]</a></sup> The <a href=\"/wiki/Ministry_of_Culture_and_Tourism_(Greece)\" class=\"mw-redirect\" title=\"Ministry of Culture and Tourism (Greece)\">Greek Ministry of Culture</a> is currently carrying out a programme of selective restoration and reconstruction to ensure the stability of the partially ruined structure.<sup id=\"cite_ref-venieri-acropolis_8-0\" class=\"reference\"><a href=\"#cite_note-venieri-acropolis-8\">[8]</a></sup></p>\n" +
                "<p>The Parthenon itself replaced an older temple of Athena, which historians call the Pre-Parthenon or <a href=\"/wiki/Older_Parthenon\" title=\"Older Parthenon\">Older Parthenon</a>, that was destroyed in the <a href=\"/wiki/Greco-Persian_wars\" class=\"mw-redirect\" title=\"Greco-Persian wars\">Persian invasion</a> of 480&nbsp;BC. The temple is <a href=\"/wiki/Archaeoastronomy\" title=\"Archaeoastronomy\">archaeoastronomically</a> aligned to the <a href=\"/wiki/Hyades_(star_cluster)\" title=\"Hyades (star cluster)\">Hyades</a>.<sup id=\"cite_ref-BoutsikasHannah2012_9-0\" class=\"reference\"><a href=\"#cite_note-BoutsikasHannah2012-9\">[9]</a></sup> Like most Greek temples, the Parthenon served a practical purpose as the city <a href=\"/wiki/Treasury\" title=\"Treasury\">treasury</a>.<sup id=\"cite_ref-10\" class=\"reference\"><a href=\"#cite_note-10\">[10]</a></sup><sup id=\"cite_ref-11\" class=\"reference\"><a href=\"#cite_note-11\">[11]</a></sup> For a time, it served as the treasury of the <a href=\"/wiki/Delian_League\" title=\"Delian League\">Delian League</a>, which later became the <a href=\"/wiki/Athenian_Empire\" class=\"mw-redirect\" title=\"Athenian Empire\">Athenian Empire</a>. In the final decade of the sixth century AD, the Parthenon was converted into a <a href=\"/wiki/History_of_Christianity#Early_Middle_Ages_.28476.E2.80.93799.29\" title=\"History of Christianity\">Christian</a> church dedicated to the <a href=\"/wiki/Mary,_the_mother_of_Jesus\" class=\"mw-redirect\" title=\"Mary, the mother of Jesus\">Virgin Mary</a>.</p>\n" +
                "<p>After the <a href=\"/wiki/Ottoman_Greece\" title=\"Ottoman Greece\">Ottoman conquest</a>, it was turned into a <a href=\"/wiki/Mosque\" title=\"Mosque\">mosque</a> in the early 1460s. On 26 September 1687, an Ottoman ammunition dump inside the building was ignited by <a href=\"/wiki/Republic_of_Venice\" title=\"Republic of Venice\">Venetian</a> bombardment. The resulting explosion severely damaged the Parthenon and its sculptures. From 1800 to 1803,<sup id=\"cite_ref-12\" class=\"reference\"><a href=\"#cite_note-12\">[12]</a></sup> <a href=\"/wiki/Thomas_Bruce,_7th_Earl_of_Elgin\" title=\"Thomas Bruce, 7th Earl of Elgin\">Thomas Bruce, 7th Earl of Elgin</a> removed some of the surviving sculptures with the alleged permission of the <a href=\"/wiki/Ottoman_Empire\" title=\"Ottoman Empire\">Ottoman Empire</a>.<sup class=\"noprint Inline-Template Template-Fact\" style=\"white-space:nowrap;\">[<i><a href=\"/wiki/Wikipedia:Citation_needed\" title=\"Wikipedia:Citation needed\"><span title=\"This claim needs references to reliable sources. (July 2016)\">citation needed</span></a></i>]</sup> These sculptures, now known as the <a href=\"/wiki/Elgin_Marbles\" title=\"Elgin Marbles\">Elgin Marbles</a> or the Parthenon Marbles, were sold in 1816 to the <a href=\"/wiki/British_Museum\" title=\"British Museum\">British Museum</a> in <a href=\"/wiki/London\" title=\"London\">London</a>, where they are now displayed. Since 1983 (on the initiative of Culture Minister <a href=\"/wiki/Melina_Mercouri\" title=\"Melina Mercouri\">Melina Mercouri</a>), the Greek government has been committed to the return of the sculptures to Greece.<sup id=\"cite_ref-13\" class=\"reference\"><a href=\"#cite_note-13\">[13]</a></sup></p>";
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setRequestProperty("Connection", "close");
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bmp = BitmapFactory.decodeStream(input);
            return bmp;
        } catch (IOException e) {
            // Log exception
            Log.e("Bitmap Download", e.getMessage());
            return null;
        }
    }

    public static String getResource(String src) {
        try {
            URL url = new URL(src);
            URLConnection connection = url.openConnection();
            connection.setRequestProperty("Connection", "close");
            connection.connect();
            InputStream is = connection.getInputStream();
            return inputStreamToString(is);
        } catch (Exception e) {
            Log.e("getResouce", e.getMessage());
            return null;
        }

    }

    public static String inputStreamToString(InputStream is) {
        StringBuilder result = new StringBuilder();
        try {
            BufferedReader bufr = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = bufr.readLine()) != null) result.append(line);
            bufr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    public static String normaliseWikiTitle(String title) {
        return title.replace(' ', '_');
    }

    public static String cleanHtml(String html) {
//        html = html.replaceAll("&amp;#xD;","</br>");
//        html = html.replaceAll("<(.*?)\\>"," ");//Removes all items in brackets
//        html = html.replaceAll("<(.*?)\\\n"," ");//Must be undeneath
//        html = html.replaceFirst("(.*?)\\>", " ");//Removes any connected item to the last bracket
//        html = html.replaceAll("&nbsp;"," ");
//        html = html.replaceAll("&amp;"," ");
//        html = html.replaceAll("\\[[1-9]\\]", "");
//        html = html.replaceAll("<h[0-9]>([\\s\\S]*)</h[0-9]>+", "");

        return html;
    }


    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }
//    public static void changeToTheme(Activity activity, int theme)
//    {
//        sTheme = theme;
//        activity.finish();
//        activity.startActivity(new Intent(activity, activity.getClass()));
//    }
//    /** Set the theme of the activity, according to the configuration. */
//    public static void onActivityCreateSetTheme(Activity activity)
//    {
//        switch (sTheme)
//        {
//            default:
//            case THEME_DEFAULT:
//                activity.setTheme(R.style.AppTheme);
//                break;
//            case THEME_DARK:
//                activity.setTheme(R.style.AppTheme_Dark);
//                break;
//        }
//    }





}
