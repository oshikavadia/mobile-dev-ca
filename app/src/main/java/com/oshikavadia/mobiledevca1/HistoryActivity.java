package com.oshikavadia.mobiledevca1;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends baseActivity {
    private DatabaseHelper db = new DatabaseHelper(this);
    private ArrayAdapter adapter;
    private List<String> historyList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        historyList = getHistoryArticles();
        ListView lv = (ListView) findViewById(R.id.history_list);
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1, historyList);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            String title = historyList.get(position);
            Intent i = new Intent(this, article_content.class);
            i.putExtra("article", title);
            startActivity(i);
        });
    }

    public void clearHistory(View v) {
        db.deleteAllArticle();
        historyList.clear();
        historyList.addAll(getHistoryArticles());
        adapter.notifyDataSetChanged();
    }

    private List<String> getHistoryArticles() {
        List<Articles> articles = db.getAllArticles();
        List<String> names = new ArrayList<>();
        for (Articles article : articles) {
            names.add(article.getArticleName());
        }
        return names;
    }

}
