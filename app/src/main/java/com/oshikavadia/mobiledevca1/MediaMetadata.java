package com.oshikavadia.mobiledevca1;

import android.graphics.Bitmap;

/**
 * Created by Oshi on 12-Nov-17.
 */

public class MediaMetadata {
    private String name;
    private String url;
    private Bitmap image;

    public MediaMetadata(String name, String url, Bitmap image) {
        this.name = name;
        this.url = url;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MediaMetadata that = (MediaMetadata) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        return image != null ? image.equals(that.image) : that.image == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (image != null ? image.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MediaMetadata{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", image=" + image +
                '}';
    }
}
