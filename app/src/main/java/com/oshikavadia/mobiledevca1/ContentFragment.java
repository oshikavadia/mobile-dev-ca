package com.oshikavadia.mobiledevca1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.ms.square.android.expandabletextview.ExpandableTextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.Format;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ContentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ContentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContentFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String FRAGMENT_NAME = "name";
    private static final String FRAGMENT_NUMBER = "number";

    private String name;
    private int number;
    private String title;
    private LinearLayout lv;
    private LayoutInflater inflater;
    private ViewGroup container;
    private OnFragmentInteractionListener mListener;
    private View view;
    private HashMap<String, Integer> contentIndexMap;
    private String coordinates[] = null;
    public ContentFragment() {
        // Required empty public constructor
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param name     Name of the fragment screen.
     * @param position position of the fragment tab.
     * @return A new instance of fragment ContentFragment.
     */
    public static ContentFragment newInstance(String name, int position) {
        ContentFragment fragment = new ContentFragment();
        Bundle args = new Bundle();
        args.putString(FRAGMENT_NAME, name);
        args.putInt(FRAGMENT_NUMBER, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(FRAGMENT_NAME);
            number = getArguments().getInt(FRAGMENT_NUMBER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.inflater = inflater;
        this.container = container;
        contentIndexMap = new HashMap<>();
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_content, container, false);
        ScrollView sv = (ScrollView) view.findViewById(R.id.content_scroll_view);
        lv = sv.findViewById(R.id.scroll_linear_view);


        title = ((article_content) getActivity()).getArticleTitle();
        new getArticleData().execute();
        return view;

    }

    private void addSection(int index, String title, String data) {
        View expTextItem = inflater.inflate(R.layout.expandable_text_item, container, false);
        lv.addView(expTextItem, index);
        ((TextView) expTextItem.findViewById(R.id.title)).setText(title);
        ((ExpandableTextView) expTextItem.findViewById(R.id.expand_text_view)).setText(Html.fromHtml(data).toString());
    }

    public class getArticleData extends AsyncTask<Void, Void, List<articleSectionContent>> {
        //
        @Override
        protected List<articleSectionContent> doInBackground(Void... params) {
            List<articleSectionContent> sectionData = new ArrayList<>();

            try {
                String json = Utils.getResource(String.format(Constants.WikipediaGetExtractForArticle, title));
                JSONObject jObject = new JSONObject(json);
                Iterator<String> iter = jObject.getJSONObject("query").getJSONObject("pages").keys();
                String pageId = "";
                if (iter.hasNext()) {
                    pageId = iter.next();
                }
                String extract = jObject.getJSONObject("query").getJSONObject("pages").getJSONObject(pageId).getString("extract");
                sectionData.add(new articleSectionContent("Extract", extract));
                json = Utils.getResource(String.format(Constants.WikipediaGetSectionsForArticle, title));

                jObject = new JSONObject(json);
                JSONArray sections = jObject.getJSONObject("parse").getJSONArray("sections");
                for (int i = 0; i < sections.length(); i++) {
                    JSONObject jsonObj = sections.getJSONObject(i);
                    String name = jsonObj.getString("line");
                    if (!name.equalsIgnoreCase("references") && !name.equalsIgnoreCase("external links") && !name.equalsIgnoreCase("further reading")) {

                        int index = jsonObj.getInt("index");
                        contentIndexMap.put(name, index);
                        @SuppressLint("DefaultLocale") String data = new JSONObject(Utils.getResource(
                                String.format(Constants.WikipediaGetDataBySection, title, index))).getJSONObject("parse").getJSONObject("text").getString("*");
                        data = Utils.cleanHtml(data);
                        Log.i("Section data", name);
                        String pattern = "[+-]?\\d*\\.\\d+(?![-+0-9\\\\.]); [+-]?\\d*\\.\\d+(?![-+0-9\\\\.])";
                        Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
                        Matcher m = r.matcher(data);
                        if (m.find()) {
                            coordinates = m.group(0).split("; ");
                        }
                        sectionData.add(new articleSectionContent(name, data));
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return sectionData;
        }

        @Override
        protected void onPreExecute() {
            view.findViewById(R.id.content_progress_bar).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(List<articleSectionContent> results) {
            view.findViewById(R.id.content_progress_bar).setVisibility(View.GONE);
            if (coordinates != null) {
                FloatingActionButton fab = view.findViewById(R.id.content_map);
                fab.setVisibility(View.VISIBLE);
                fab.setOnClickListener(v -> {
                    String uri = String.format(Locale.ENGLISH, "geo:%s,%s", coordinates[0], coordinates[1]);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    startActivity(intent);

                });
            }
            int i = 0;
            for (articleSectionContent articleSectionContent : results) {
                addSection(i, articleSectionContent.getSectionName(), articleSectionContent.getSectionData());
                i++;
            }

        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
