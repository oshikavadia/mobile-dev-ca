package com.oshikavadia.mobiledevca1;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.util.HashMap;

public class SavedArticleContent extends baseActivity {
    private String title;
    private LinearLayout lv;
    private LayoutInflater inflater;
    private Saved saved;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_article_content);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        inflater = getLayoutInflater();
        saved = (Saved) getIntent().getSerializableExtra("saved");
        ScrollView sv = (ScrollView) findViewById(R.id.content_scroll_view);
        lv = sv.findViewById(R.id.scroll_linear_view);


        title = saved.getSaveName();
        HashMap<String, String> content = saved.getContent();
        int count = 0;
        for (String key : content.keySet()) {
            addSection(count, key, content.get(key));
            count++;
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void addSection(int index, String title, String data) {
//        ViewGroup view = (ViewGroup) findViewById(android.R.id.content);
        View expTextItem = inflater.inflate(R.layout.expandable_text_item, null, false);
        lv.addView(expTextItem, index);
        ((TextView) expTextItem.findViewById(R.id.title)).setText(title);
        ((ExpandableTextView) expTextItem.findViewById(R.id.expand_text_view)).setText(Html.fromHtml(data).toString());
    }

}
