package com.oshikavadia.mobiledevca1;

/**
 * Created by Shane on 02/12/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

import static java.util.Base64.getEncoder;

public class DatabaseHelper extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 8;

    // Database Name
    private static final String DATABASE_NAME = "wikiDB";

    // Table names
    private static final String TABLE_ARTICLES = "articles";
    private static final String TABLE_SEARCH = "search";
    private static final String TABLE_PINNED = "pinned";
    private static final String TABLE_SAVED = "saved";
    private static final String TABLE_TAGS = "tags";

    // Articles Table Columns names
    private static final String ARTICLEID = "_id";
    private static final String ARTICLENAME = "articleName";

    // Search Table Columns names
    private static final String SEARCHID = "_id";
    private static final String SEARCHKEY = "searchKey";
    private static final String SEARCHDATE = "date";

    // Pinned Articles Table Columns names
    private static final String PINNEDID = "_id";
    private static final String PINNEDNAME = "pinName";
    private static final String PINNEDNORMNAME = "pinNormName";

    // Saved Articles Table Columns names
    private static final String SAVEDID = "_id";
    private static final String SAVEDNAME = "saveName";
    private static final String SAVEDURL = "saveURL";
    private static final String SAVEDCONTENT = "content";

    // Tag Articles Table Columns names
    private static final String TAGID = "_id";
    private static final String TAGKEY = "tag";


    // Table Create Statements
    private static final String CREATE_TABLE_ARTICLE = "CREATE TABLE "
            + TABLE_ARTICLES + "(" + ARTICLEID + " INTEGER PRIMARY KEY," + ARTICLENAME
            + " TEXT" + ")";

    private static final String CREATE_TABLE_SEARCH = "CREATE TABLE "
            + TABLE_SEARCH + "(" + SEARCHID + " INTEGER PRIMARY KEY," + SEARCHKEY
            + " TEXT, " + SEARCHDATE + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP" + ")";

    private static final String CREATE_TABLE_PINNED = "CREATE TABLE "
            + TABLE_PINNED + "(" + PINNEDID + " INTEGER PRIMARY KEY," + PINNEDNAME
            + " TEXT, " + PINNEDNORMNAME + " TEXT" + ")";

    private static final String CREATE_TABLE_SAVED = "CREATE TABLE "
            + TABLE_SAVED + "(" + SAVEDID + " INTEGER PRIMARY KEY," + SAVEDNAME
            + " TEXT, " + SAVEDURL + " TEXT," + SAVEDCONTENT + " BLOB" + ")";

    private static final String CREATE_TABLE_TAGS = "CREATE TABLE "
            + TABLE_TAGS + "(" + TAGID + " INTEGER PRIMARY KEY," + TAGKEY
            + " TEXT" + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // creating required tables
        db.execSQL(CREATE_TABLE_ARTICLE);
        db.execSQL(CREATE_TABLE_SEARCH);
        db.execSQL(CREATE_TABLE_PINNED);
        db.execSQL(CREATE_TABLE_SAVED);
        db.execSQL(CREATE_TABLE_TAGS);

    }


    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTICLES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SEARCH);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PINNED);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SAVED);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAGS);

        // Create tables again
        onCreate(db);
    }

    /**
     * All ARTICLE CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new article
    void addArticle(Articles article) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ARTICLENAME, article.getArticleName()); // Article Name

        // Inserting Row
        db.insert(TABLE_ARTICLES, null, values);
        db.close(); // Closing database connection
    }

    // Getting single article
    Articles getArticle(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ARTICLES, new String[]{ARTICLEID,
                        ARTICLENAME}, ARTICLEID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Articles article = new Articles(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1));
        return article;
    }

    // Getting All articles
    public List<Articles> getAllArticles() {
        List<Articles> articleList = new ArrayList<Articles>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ARTICLES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Articles article = new Articles();
                article.set_id(Integer.parseInt(cursor.getString(0)));
                article.setArticleName(cursor.getString(1));
                // Adding contact to list
                articleList.add(article);
            } while (cursor.moveToNext());
        }

        return articleList;
    }

    // Updating single article
    public int updateArticle(Articles article) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ARTICLENAME, article.getArticleName());

        // updating row
        return db.update(TABLE_ARTICLES, values, ARTICLEID + " = ?",
                new String[]{String.valueOf(article.get_id())});
    }

    // Deleting single article
    public void deleteArticle(Articles article) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ARTICLES, ARTICLEID + " = ?",
                new String[]{String.valueOf(article.get_id())});
        db.close();
    }

    // Deleting all article
    public void deleteAllArticle() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_ARTICLES);
        db.close();
    }


    // Getting article Count
    public int getArticlesCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ARTICLES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        return cursor.getCount();
    }

    /**
     * All SEARCH CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new search
    void addSearch(Search search) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SEARCHKEY, search.getSearchKey());
        values.put(SEARCHDATE, search.getDate());

        // Inserting Row
        db.insert(TABLE_SEARCH, null, values);
        db.close(); // Closing database connection
    }

    // Getting single search
    Search getSearch(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SEARCH, new String[]{SEARCHID,
                        SEARCHKEY, SEARCHDATE}, SEARCHID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Search search = new Search(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        return search;
    }

    // Getting All search's
    public List<Search> getAllSearchs() {
        List<Search> searchList = new ArrayList<Search>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SEARCH;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Search search = new Search();
                search.set_id(Integer.parseInt(cursor.getString(0)));
                search.setSearchKey(cursor.getString(1));
                search.setDate(cursor.getString(2));
                // Adding contact to list
                searchList.add(search);
            } while (cursor.moveToNext());
        }

        return searchList;
    }

    public void deleteAllSearches() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_SEARCH);
        db.close();
    }

    // Updating single search
    public int updateSearch(Search search) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SEARCHKEY, search.getSearchKey());
        values.put(SEARCHDATE, search.getDate());

        // updating row
        return db.update(TABLE_SEARCH, values, SEARCHID + " = ?",
                new String[]{String.valueOf(search.get_id())});
    }

    // Deleting single search
    public void deleteSearch(Search search) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SEARCH, SEARCHID + " = ?",
                new String[]{String.valueOf(search.get_id())});
        db.close();
    }


    // Getting search count
    public int getSearchCount() {
        String countQuery = "SELECT  * FROM " + TABLE_SEARCH;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        return cursor.getCount();
    }

    /**
     * All PINNED CRUD(Create, Read, Update, Delete) Operations
     */
    // Adding new pinned article
    void addPinArticle(Pinned pinned) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PINNEDNAME, pinned.getPinName());
        values.put(PINNEDNORMNAME, pinned.getPinNormName());

        // Inserting Row
        db.insert(TABLE_PINNED, null, values);
        db.close(); // Closing database connection
    }


    // Get pinned article
    Pinned getPinArticle(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_PINNED, new String[]{PINNEDID,
                        PINNEDNAME, PINNEDNORMNAME}, PINNEDID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Pinned pinned = new Pinned(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1));
        return pinned;
    }

    // Getting All pinned articles
    public List<Pinned> getAllPinArticles() {
        List<Pinned> articleList = new ArrayList<Pinned>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_PINNED;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Pinned pinned = new Pinned();
                pinned.set_id(Integer.parseInt(cursor.getString(0)));
                pinned.setPinName(cursor.getString(1));
                pinned.setPinNormName(cursor.getString(2));
                articleList.add(pinned);
            } while (cursor.moveToNext());
        }

        return articleList;
    }

    // Deleting single pinned article
    public void deletePinArticle(Pinned pinned) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_PINNED, PINNEDID + " = ?",
                new String[]{String.valueOf(pinned.get_id())});
        db.close();
    }

    // Deleting all pinned article
    public void deleteAllPinArticle() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_PINNED);
        db.close();
    }


    /**
     * All SAVED CRUD(Create, Read, Update, Delete) Operations
     */
    // Adding new saved article
    void addSavedArticle(Saved saved) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SAVEDNAME, saved.getSaveName());
        values.put(SAVEDURL, saved.getUrlName());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        String serialisedObject = "";
        try {
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(saved.getContent());
            oos.close();
            serialisedObject = android.util.Base64.encodeToString(baos.toByteArray(), android.util.Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }

        values.put(SAVEDCONTENT, serialisedObject);

        // Inserting Row
        db.insert(TABLE_SAVED, null, values);
        db.close(); // Closing database connection
    }

    // Get pinned article
    public Saved getSavedArticle(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_SAVED, new String[]{SAVEDID,
                        SAVEDNAME, SAVEDURL, SAVEDCONTENT}, SAVEDID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Object o = null;
        try {
            byte[] data = android.util.Base64.decode(cursor.getString(1), android.util.Base64.DEFAULT);
            ObjectInputStream ois = new ObjectInputStream(
                    new ByteArrayInputStream(data));
            o = ois.readObject();
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Saved saved = new Saved(Integer.parseInt(cursor.getString(0)), (HashMap<String, String>) o);
        return saved;
    }

    public HashMap<Integer, String> getSavedArticlesNameAndIds() {
        return null;
    }

    // Getting All pinned articles
    public List<Saved> getAllSavedArticles() {
        List<Saved> articleList = new ArrayList<Saved>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_SAVED;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Saved saved = new Saved();
                saved.set_id(Integer.parseInt(cursor.getString(0)));
                saved.setSaveName(cursor.getString(1));
                saved.setUrlName(cursor.getString(2));
                Object o = null;
                try {
                    byte[] data = android.util.Base64.decode(cursor.getString(cursor.getColumnIndex(SAVEDCONTENT)), android.util.Base64.DEFAULT);
                    ObjectInputStream ois = new ObjectInputStream(
                            new ByteArrayInputStream(data));
                    o = ois.readObject();
                    ois.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                      saved.setContent(cursor.getBlob());
                saved.setContent((HashMap<String, String>) o);
                articleList.add(saved);
            } while (cursor.moveToNext());
        }

        return articleList;
    }

    // Deleting single saved article
    public void deleteSavedArticle(Saved saved) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SAVED, SAVEDID + " = ?",
                new String[]{String.valueOf(saved.get_id())});
        db.close();
    }

    // Deleting all saved article
    public void deleteAllSavedArticle() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_SAVED);
        db.close();
    }

    /**
     * All TAG CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new tag
    void addTag(Tags tags) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TAGKEY, tags.getTag());

        // Inserting Row
        db.insert(TABLE_TAGS, null, values);
        db.close(); // Closing database connection
    }


    // Getting single tag
    Tags getTag(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_TAGS, new String[]{TAGID,
                        TAGKEY}, TAGID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Tags tag = new Tags(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1));
        return tag;
    }

    // Getting All tags
    public List<Tags> getAllTags() {
        List<Tags> tagList = new ArrayList<Tags>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TAGS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Tags tag = new Tags();
                tag.set_id(Integer.parseInt(cursor.getString(0)));
                tag.setTag(cursor.getString(1));
                // Adding contact to list
                tagList.add(tag);
            } while (cursor.moveToNext());
        }

        return tagList;
    }

    // Updating single tag
    public int updateTag(Tags tag) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TAGKEY, tag.getTag());

        // updating row
        return db.update(TABLE_TAGS, values, TAGID + " = ?",
                new String[]{String.valueOf(tag.get_id())});
    }

    // Deleting single tag
    public void deleteTag(Tags tag) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TAGS, TAGID + " = ?",
                new String[]{String.valueOf(tag.get_id())});
        db.close();
    }

    public int getIdForTag(String tag) {
        SQLiteDatabase db = this.getWritableDatabase();
        String q = String.format("select %s from %s where %s = '%s'", TAGID, TABLE_TAGS, TAGKEY, tag);
        Cursor cursor = db.rawQuery(q, null);
        int id = -1;
        if (cursor.moveToFirst()) {
            id = cursor.getInt(0);
        }
        return id;
    }

    public int getLastInsertId(){
        SQLiteDatabase db = this.getReadableDatabase();
        String q = "select last_insert_rowid();";
        Cursor cursor = db.rawQuery(q, null);
        int id = -1;
        if (cursor.moveToFirst()) {
            id = cursor.getInt(0);
        }
        return  id;
    }

    // Deleting single tag
    public void deleteTag(int tagID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TAGS, TAGID + " = ?",
                new String[]{String.valueOf(tagID)});
        db.close();
    }

    // Deleting all tags
    public void deleteAllTags() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_TAGS);
        db.close();
    }


    // Getting tag count
    public int getTagCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ARTICLES;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        return cursor.getCount();
    }


}

//References https://www.androidhive.info/2013/09/android-sqlite-database-with-multiple-tables/