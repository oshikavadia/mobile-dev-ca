package com.oshikavadia.mobiledevca1;

import java.io.Serializable;
import java.sql.Blob;
import java.util.HashMap;

/**
 * Created by Shane on 03/03/2018.
 */

public class Tags
{
    private int _id;
    private String tag;

    public Tags(int _id, String tag)
    {
        this._id = _id;
        this.tag = tag;
    }

    public Tags()
    {

    }

    public int get_id()
    {
        return _id;
    }

    public String getTag()
    {
        return tag;
    }

    public void set_id(int _id)
    {
        this._id = _id;
    }

    public void setTag(String tag)
    {
        this.tag = tag;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tags tags = (Tags) o;

        if (_id != tags._id) return false;
        return tag != null ? tag.equals(tags.tag) : tags.tag == null;
    }

    @Override
    public int hashCode()
    {
        int result = _id;
        result = 31 * result + (tag != null ? tag.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return "Tags{" +
                "_id=" + _id +
                ", tag='" + tag + '\'' +
                '}';
    }

}

