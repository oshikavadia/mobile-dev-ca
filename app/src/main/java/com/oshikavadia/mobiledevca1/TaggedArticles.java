package com.oshikavadia.mobiledevca1;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Shane on 03/03/2018.
 */

public class TaggedArticles extends baseActivity {
    private DatabaseHelper db = new DatabaseHelper(this);
    private List<String> taggedArticles = new ArrayList<>();
    private ArrayAdapter adapter;
    private HashMap<String, Integer> tagMap = new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tagged);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        taggedArticles.addAll(getTags());
        ListView lv = (ListView) findViewById(R.id.tags_list);
        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, android.R.id.text1, taggedArticles);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            db.deleteTag(tagMap.get(taggedArticles.get(position)));
            taggedArticles.remove(position);
            adapter.notifyDataSetChanged();
        });
    }

    private List<String> getTags() {
        List<Tags> tag = db.getAllTags();
        List<String> keys = new ArrayList<>();
        for (Tags taggedArticles : tag) {
            tagMap.put(taggedArticles.getTag(),taggedArticles.get_id() );
            keys.add(taggedArticles.getTag());
        }
        return keys;
    }

    public void addTag(View view) {
        EditText editText = (EditText) findViewById(R.id.editText1);
        String key = editText.getText().toString();
        Tags tag = new Tags();
        tag.setTag(key);
        db.addTag(tag);
        taggedArticles.add(tag.getTag());
        tagMap.put(tag.getTag(),db.getIdForTag(tag.getTag()));
        adapter.notifyDataSetChanged();
    }


}
