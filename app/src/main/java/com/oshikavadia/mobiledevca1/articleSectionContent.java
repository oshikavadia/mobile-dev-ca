package com.oshikavadia.mobiledevca1;

/**
 * Created by Oshi on 27-Nov-17.
 */

public class articleSectionContent {
    private String sectionName, sectionData;

    articleSectionContent(String name, String data) {
        this.sectionName = name;
        this.sectionData = data;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getSectionData() {
        return sectionData;
    }

    public void setSectionData(String sectionData) {
        this.sectionData = sectionData;
    }
}
